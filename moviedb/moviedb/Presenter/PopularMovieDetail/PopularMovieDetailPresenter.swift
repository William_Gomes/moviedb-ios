import Foundation

class PopularMovieDetailPresenter: PopularMovieDetailPresenterProtocol {

    var popularMovie: PopularMovie

    required init(popularMovie: PopularMovie) {

        self.popularMovie = popularMovie
    }

    func formatGenreNames() -> String? {

        let genreNames = popularMovie.genres?.map({ genre in
            genre.name
        })

        return genreNames?.joined(separator: ", ")
    }
}
