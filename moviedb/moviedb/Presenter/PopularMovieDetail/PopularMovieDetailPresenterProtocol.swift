import Foundation

protocol PopularMovieDetailPresenterProtocol {

    var popularMovie: PopularMovie { get }

    init(popularMovie: PopularMovie)

    func formatGenreNames() -> String?
}
