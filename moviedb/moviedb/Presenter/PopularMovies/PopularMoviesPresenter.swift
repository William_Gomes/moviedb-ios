import Foundation

class PopularMoviesPresenter: PopularMoviesPresenterProtocol {

    private(set) var popularMovies: [PopularMovie] = []
    private(set) var selectedPopularMovie: PopularMovie?
    private(set) var genres: [Genre] = []
    private(set) weak var delegate: PopularMoviesPresenterDelegate?
    private(set) var isLoading = false
    private(set) var totalPages = 0
    private(set) var currentPage = 0

    var hasMoreContentToLoad: Bool {

        return currentPage < totalPages
    }

    init(delegate: PopularMoviesPresenterDelegate) {

        self.delegate = delegate
    }

    func getPopularMovies() {

        if !isLoading {

            isLoading = true
            getGenres(onSuccess: {

                self.requestPopularMovies(refresh: true)

            }, onError: { error in

                self.isLoading = false
                self.delegate?.didFailGetPopularMovies(with: error)
            })
        }
    }

    func getMorePopularMovies() {

        if !isLoading && hasMoreContentToLoad {

            requestPopularMovies()
        }
    }

    private func requestPopularMovies(refresh: Bool = false) {

        isLoading = true
        currentPage = refresh ? 1 : currentPage + 1

        let request = RequestModelGetPopularMovies(page: currentPage)
        let requestAdapter = MovieDBRequestAdapter(requestModelProtocol: request)
        let parser = ModelParserPopularMovies()

        APIRequest().execute(requestModelAdapter: requestAdapter,
                             parserModel: parser,
                             onSuccess: { pagedResponse in

                                self.totalPages = pagedResponse.numberOfPages
                                self.isLoading = false

                                if refresh {

                                    self.popularMovies = pagedResponse.results

                                } else {

                                    self.popularMovies.append(contentsOf: pagedResponse.results)
                                }

                                self.delegate?.didSuccessGetPopularMovies()

        }, onError: { error in

            self.isLoading = false
            self.delegate?.didFailGetPopularMovies(with: error)
        })
    }

    private func getGenres(onSuccess: @escaping () -> Void,
                           onError: @escaping (ServiceError) -> Void) {

        let request = RequestModelGetGenres()
        let requestAdapter = MovieDBRequestAdapter(requestModelProtocol: request)
        let parser = ModelParserGenres()

        APIRequest().execute(requestModelAdapter: requestAdapter,
                             parserModel: parser,
                             onSuccess: { genres in

                                self.genres = genres
                                onSuccess()

        }, onError: { error in

            onError(error)
        })
    }

    func selectMovie(at indexPath: IndexPath) {

        selectedPopularMovie = popularMovies[indexPath.row]

        selectedPopularMovie?.genres = selectedPopularMovie?.genreIds.compactMap({ genreID in

            let genre = genres.first(where: { genre in
                return genre.identifier == genreID
            })

            return genre
        })
    }
}
