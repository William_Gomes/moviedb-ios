import Foundation

protocol PopularMoviesPresenterProtocol {

    var delegate: PopularMoviesPresenterDelegate? { get }
    var popularMovies: [PopularMovie] { get }
    var selectedPopularMovie: PopularMovie? { get }
    var isLoading: Bool { get }
    var hasMoreContentToLoad: Bool { get }

    func getPopularMovies()
    func getMorePopularMovies()
    func selectMovie(at indexPath: IndexPath)
}
