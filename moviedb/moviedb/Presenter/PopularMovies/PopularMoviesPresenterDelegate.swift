import Foundation

protocol PopularMoviesPresenterDelegate: class {

    func didSuccessGetPopularMovies()
    func didFailGetPopularMovies(with error: ServiceError)
}
