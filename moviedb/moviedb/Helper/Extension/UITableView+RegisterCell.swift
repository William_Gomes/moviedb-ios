import UIKit

extension UITableView {

    public func register<T: UITableViewCell>(_ cellClass: T.Type) {

        register(cellClass, forCellReuseIdentifier: cellClass.reuseIdentifier)
    }

    public func registerNib<T: UITableViewCell>(for cellClass: T.Type, in bundle: Bundle? = nil) {

        register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
    }

    public func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {

        return dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
}
