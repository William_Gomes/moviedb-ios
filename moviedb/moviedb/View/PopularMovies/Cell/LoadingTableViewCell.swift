import UIKit

class LoadingTableViewCell: UITableViewCell {

    @IBOutlet private weak var loadingActivityIndicatorView: UIActivityIndicatorView!

    override func prepareForReuse() {

        super.prepareForReuse()
        loadingActivityIndicatorView.startAnimating()
    }
}
