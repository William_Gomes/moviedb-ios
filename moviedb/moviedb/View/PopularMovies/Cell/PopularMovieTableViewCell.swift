import Kingfisher
import UIKit

class PopularMovieTableViewCell: UITableViewCell {

    @IBOutlet private weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var popularityLabel: UILabel!
    @IBOutlet private weak var releaseYearLabel: UILabel!

    override func prepareForReuse() {

        super.prepareForReuse()
        thumbnailImageView.image = nil
    }

    func setCell(with popularMovie: PopularMovie) {

        titleLabel.text = popularMovie.title
        popularityLabel.text = String(popularMovie.popularity)
        releaseYearLabel.text = popularMovie.releaseYear

        if let posterPath = popularMovie.posterPath {

            let baseURL = PlistFiles.apiMovieDBImageBaseURL
            let thumbnailPath = "\(baseURL)\(MovieDBApiPath.Image.thumbnail.rawValue)\(posterPath)"
            let thumbnailPathURL = URL(string: thumbnailPath)

            thumbnailImageView.kf.setImage(with: thumbnailPathURL,
                                           placeholder: Asset.popularMoviePlaceholder.image)

        } else {

            thumbnailImageView.image = Asset.popularMoviePlaceholder.image
        }
    }
}
