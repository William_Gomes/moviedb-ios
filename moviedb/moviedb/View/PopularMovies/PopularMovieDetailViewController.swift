import UIKit

class PopularMovieDetailViewController: UIViewController {

    @IBOutlet private weak var thumbnailImageView: UIImageView!
    @IBOutlet private weak var posterImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var releaseYearLabel: UILabel!
    @IBOutlet private weak var popularityLabel: UILabel!
    @IBOutlet private weak var genreLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!

    var presenter: PopularMovieDetailPresenterProtocol!

    override func viewDidLoad() {

        super.viewDidLoad()

        titleLabel.text = presenter.popularMovie.title
        releaseYearLabel.text = presenter.popularMovie.releaseYear
        popularityLabel.text = String(presenter.popularMovie.popularity)
        genreLabel.text = presenter.formatGenreNames()
        overviewLabel.text = presenter.popularMovie.overview

        let baseURL = PlistFiles.apiMovieDBImageBaseURL

        if let posterPath = presenter.popularMovie.posterPath {

            let thumbnailPath = "\(baseURL)\(MovieDBApiPath.Image.thumbnail.rawValue)\(posterPath)"
            let thumbnailPathURL = URL(string: thumbnailPath)
            thumbnailImageView.kf.setImage(with: thumbnailPathURL,
                                           placeholder: Asset.popularMoviePlaceholder.image)
        } else {

            thumbnailImageView.image = Asset.popularMoviePlaceholder.image
        }

        if let backdropPath = presenter.popularMovie.backdropPath {

            let posterPath = "\(baseURL)\(MovieDBApiPath.Image.big.rawValue)\(backdropPath)"
            let posterPathURL = URL(string: posterPath)
            posterImageView.kf.setImage(with: posterPathURL,
                                        placeholder: Asset.popularMoviePlaceholder.image)
        } else {

            posterImageView.image = Asset.popularMoviePlaceholder.image
        }
    }
}
