import UIKit

class PopularMoviesViewController: UIViewController {

    @IBOutlet private weak var popularMoviesTableView: UITableView!

    private var presenter: PopularMoviesPresenterProtocol!

    private lazy var refreshControl: UIRefreshControl = {

        return UIRefreshControl()
    }()

    override func viewDidLoad() {

        super.viewDidLoad()

        setupTableView()
        presenter = PopularMoviesPresenter(delegate: self)
        popularMoviesTableView.isUserInteractionEnabled = false
        presenter.getPopularMovies()
    }

    @objc
    private func refresh() {

        presenter.getPopularMovies()
    }

    private func setupTableView() {

        popularMoviesTableView.registerNib(for: PopularMovieTableViewCell.self)
        popularMoviesTableView.registerNib(for: LoadingTableViewCell.self)
        popularMoviesTableView.rowHeight = UITableView.automaticDimension
        popularMoviesTableView.estimatedRowHeight = 100

        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        popularMoviesTableView.refreshControl = refreshControl
    }

    private func reloadData() {

        refreshControl.endRefreshing()
        popularMoviesTableView.isUserInteractionEnabled = true
        popularMoviesTableView.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let popularMovieDetailViewController = segue.destination as? PopularMovieDetailViewController,
            let selectedPopularMovie = presenter.selectedPopularMovie {

            let presenter = PopularMovieDetailPresenter(popularMovie: selectedPopularMovie)
            popularMovieDetailViewController.presenter = presenter
        }
    }
}

extension PopularMoviesViewController: PopularMoviesPresenterDelegate {

    func didSuccessGetPopularMovies() {

        reloadData()
    }

    func didFailGetPopularMovies(with error: ServiceError) {

        reloadData()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {

            self.showMessage(with: error)
        }
    }
}

extension PopularMoviesViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return presenter.hasMoreContentToLoad ? presenter.popularMovies.count + 1 : presenter.popularMovies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == presenter.popularMovies.count && presenter.hasMoreContentToLoad {

            let cell = tableView.dequeueReusableCell(for: indexPath) as LoadingTableViewCell
            return cell

        } else {

            let cell = tableView.dequeueReusableCell(for: indexPath) as PopularMovieTableViewCell
            let popularMovie = presenter.popularMovies[indexPath.row]

            cell.setCell(with: popularMovie)

            return cell
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if indexPath.row == presenter.popularMovies.count {

            presenter.getMorePopularMovies()
        }
    }
}

extension PopularMoviesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        presenter.selectMovie(at: indexPath)
        perform(segue: StoryboardSegue.PopularMovies.segueMovieDetail)
    }
}
