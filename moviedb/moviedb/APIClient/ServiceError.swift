import Foundation

enum ServiceError: Error {

    case invalidURL
    case noInternetConnection
    case unableToParseJSONToDictionary
    case serverError(code: String, message: String)
}
