import Foundation

enum MovieDBApiPath {

    enum EndPoint: String {

        case popularMovies = "movie/popular"
        case genres = "genre/movie/list"
    }

    enum Image: String {

        case thumbnail = "w92/"
        case big = "w500/"
    }
}
