import Foundation
import Result

enum ModelParserHelper {

    static func parseArrayData<M: Decodable>(arrayNodeName: String?, data: Data) -> Result<M, ServiceError> {

        do {

            if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {

                let jsonData: Data
                if let nodeName = arrayNodeName {

                    jsonData = try JSONSerialization.data(withJSONObject: json[nodeName] ?? [:],
                                                          options: .prettyPrinted)
                } else {

                    jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                }

                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .formatted(DateFormatter.yyyyMMddFormatter)

                let parsedModel = try decoder.decode(M.self, from: jsonData)
                return .success(parsedModel)

            } else {

                throw ServiceError.unableToParseJSONToDictionary
            }

        } catch {

            return .failure(ServiceError.unableToParseJSONToDictionary)
        }
    }
}
