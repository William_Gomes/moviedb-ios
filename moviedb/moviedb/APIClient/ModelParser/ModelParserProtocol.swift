import Foundation
import Result

protocol ModelParserProtocol {

    associatedtype Model

    func parseData(data: Data) -> Result<Model, ServiceError>
}
