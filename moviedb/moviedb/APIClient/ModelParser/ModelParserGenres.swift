import Foundation
import Result

class ModelParserGenres: ModelParserProtocol {

    func parseData(data: Data) -> Result<[Genre], ServiceError> {

       return ModelParserHelper.parseArrayData(arrayNodeName: "genres", data: data)
    }
}
