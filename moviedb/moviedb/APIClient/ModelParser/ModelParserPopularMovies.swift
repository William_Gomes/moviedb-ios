import Foundation
import Result

class ModelParserPopularMovies: ModelParserProtocol {

    func parseData(data: Data) -> Result<PagedResponse<PopularMovie>, ServiceError> {

        return ModelParserHelper.parseArrayData(arrayNodeName: nil, data: data)
    }
}
