import Foundation

class PagedResponse<T: Decodable>: Decodable {

    let currentPage: Int
    let numberOfPages: Int
    let totalResults: Int
    let results: [T]

    enum CodingKeys: String, CodingKey {

        case currentPage = "page"
        case numberOfPages = "total_pages"
        case totalResults = "total_results"
        case results = "results"
    }
}
