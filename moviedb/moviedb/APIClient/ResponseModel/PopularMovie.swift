import Foundation

class PopularMovie: Decodable {

    let identifier: Int
    let title: String
    let voteCount: Int
    let posterPath: String?
    let backdropPath: String?
    let popularity: Double
    let releaseDate: Date
    let overview: String
    let genreIds: [Int]
    var genres: [Genre]?
    var releaseYear: String {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: releaseDate)
    }

    enum CodingKeys: String, CodingKey {

        case identifier = "id"
        case title = "original_title"
        case voteCount = "vote_count"
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case popularity = "popularity"
        case releaseDate = "release_date"
        case overview = "overview"
        case genreIds = "genre_ids"
    }
}
