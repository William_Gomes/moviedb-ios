import Alamofire
import Foundation

class APIRequest {

    private let sessionManager = SessionManager.default

    func execute<P: ModelParserProtocol>(requestModelAdapter: RequestModelAdapterProtocol,
                                         parserModel: P,
                                         onSuccess: @escaping (P.Model) -> Void,
                                         onError: @escaping (ServiceError) -> Void) {

        sessionManager.request(requestModelAdapter).responseData { (response: DataResponse<Data>) in

            self.parseResponse(response: response,
                               parserModel: parserModel,
                               onSuccess: onSuccess,
                               onError: onError)
        }
    }

    private func parseResponse<P: ModelParserProtocol>(response: DataResponse<Data>,
                                                       parserModel: P,
                                                       onSuccess: @escaping (P.Model) -> Void,
                                                       onError: @escaping (ServiceError) -> Void) {

        switch response.result {

        case .success(let data):

            let parsedModel = parserModel.parseData(data: data)

            parsedModel.analysis(ifSuccess: { responseModel in

                onSuccess(responseModel)

            }, ifFailure: { error  in

                onError(error)
            })

        case .failure(let error as NSError):

            onError(translateResponseError(error: error))
        }
    }

    private func translateResponseError(error: NSError) -> ServiceError {

        switch error.code {

        case -1009:

            return ServiceError.noInternetConnection

        default:

            return ServiceError.serverError(code: String(error.code), message: error.localizedDescription)
        }
    }
}
