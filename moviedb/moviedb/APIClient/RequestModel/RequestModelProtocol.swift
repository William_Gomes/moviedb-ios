import Alamofire
import Foundation

protocol RequestModelProtocol {

    var path: String { get }
    var parameters: [String: Any]? { get }
    var httpHeaders: [String: String]? { get }
    var httpMethod: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
}

extension RequestModelProtocol {

    var encoding: ParameterEncoding {

        return JSONEncoding.default
    }
}
