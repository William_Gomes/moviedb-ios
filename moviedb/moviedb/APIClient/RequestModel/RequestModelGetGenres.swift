import Alamofire
import Foundation

class RequestModelGetGenres: RequestModelProtocol {

    let path = MovieDBApiPath.EndPoint.genres.rawValue
    let httpMethod = HTTPMethod.get
    let httpHeaders: [String: String]? = nil
    let parameters: [String: Any]? = nil
    let encoding: ParameterEncoding = URLEncoding.queryString
}
