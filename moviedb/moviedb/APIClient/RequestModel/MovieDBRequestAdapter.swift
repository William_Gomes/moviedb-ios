import Alamofire
import Foundation

class MovieDBRequestAdapter: RequestModelAdapterProtocol {

    let requestModelProtocol: RequestModelProtocol

    init(requestModelProtocol: RequestModelProtocol) {

        self.requestModelProtocol = requestModelProtocol
    }

    func asURLRequest() throws -> URLRequest {

        var urlComponents = URLComponents(string: PlistFiles.apiMovieDBBaseURL + requestModelProtocol.path)
        urlComponents?.queryItems = [URLQueryItem(name: "api_key", value: PlistFiles.movieDBApiKey)]

        let urlRequest = try RequestAdapterFactory.buildURLRequest(requestModelProtocol: requestModelProtocol,
                                                                   urlComponents: urlComponents)

        return urlRequest
    }
}
