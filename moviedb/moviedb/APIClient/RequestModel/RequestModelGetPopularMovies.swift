import Alamofire
import Foundation

class RequestModelGetPopularMovies: RequestModelProtocol {

    let path = MovieDBApiPath.EndPoint.popularMovies.rawValue
    let httpMethod = HTTPMethod.get
    let httpHeaders: [String: String]? = nil
    let parameters: [String: Any]?
    let encoding: ParameterEncoding = URLEncoding.queryString

    init(page: Int) {

        parameters = ["page": String(page)]
    }
}
