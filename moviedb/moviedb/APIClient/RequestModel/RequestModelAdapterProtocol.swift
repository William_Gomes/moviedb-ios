import Alamofire
import Foundation

protocol RequestModelAdapterProtocol: URLRequestConvertible {

    var requestModelProtocol: RequestModelProtocol { get }
}
