import Alamofire
import Foundation

class RequestAdapterFactory {

    private init() { }

    static func buildURLRequest(requestModelProtocol: RequestModelProtocol,
                                urlComponents: URLComponents?) throws -> URLRequest {

        if let endPointURL = try urlComponents?.asURL() {

            var urlRequest = URLRequest(url: endPointURL)

            urlRequest = try requestModelProtocol.encoding.encode(urlRequest,
                                                                  with: requestModelProtocol.parameters)

            urlRequest.httpMethod = requestModelProtocol.httpMethod.rawValue

            if let httpHeaders = requestModelProtocol.httpHeaders {

                httpHeaders.forEach { httpHeader, value in

                    urlRequest.setValue(value, forHTTPHeaderField: httpHeader)
                }
            }

            return urlRequest

        } else {

            throw ServiceError.invalidURL
        }
    }
}
