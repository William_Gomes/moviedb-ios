# MovieDB iOS

### Prerequisites

To install simply run pod install

```
pod install
```

### Improvements

- Add localised strings in the project
- Show custom error messages when there's a server error
- Add logic to deal with error on pagination
- Add dependency injection
- Create a router to pass the data between view controllers
- I could use pre-fetch to gather the data but i'm stil learning how to use it
- Create a protocol to APIRequest. this way we could inject mock data
- Create a logging system.
